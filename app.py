# POUR LANCER ce fichier dans le terminal : 
# flask run (ou bien flask run --debug pour redemarrer automatiquement le serveur et surtout afficher des erreurs détaillées. par contre n'actualise pas la page du navigateur, à faire)
# Si ne marche pas faire export FLASK_APP=app (export FLASK_APP=nomdufichier). To tell your terminal the application to work with by exporting the FLASK_APP environment variable
# Devrait etre dispo à l'adresse : http://127.0.0.1:5000/
# Ctrl + C pour quitter le serveur

# importe le module Flask
from flask import Flask # First we imported the Flask class. An instance of this class will be our WSGI application.

app = Flask(__name__) # Next we create an instance of this class. The first argument is the name of the application’s module or package. __name__ is a convenient shortcut for this that is appropriate for most cases. This is needed so that Flask knows where to look for resources such as templates and static files.

# définition d'une route GET sur l'URL racine ('/')
@app.route('/') # We then use the route() decorator to tell Flask what URL should trigger our function.
def hello_world():
    # envoie une réponse 'Hello World!' au client
    return 'Bienvenue sur l\'API Pokémon ! \n Allez voir notre Pokédex sur http://127.0.0.1:5000/data' # The function returns the message we want to display in the user’s browser. The default content type is HTML, so HTML in the string will be rendered by the browser.

# Cette fois ci avec des donénes en JSON : 
from flask import jsonify

@app.route('/data') # http://127.0.0.1:5000/data
def data():
    return jsonify({'name': 'Pikachu', 'power': 20, 'life': 50}, {'name': 'Carapuce', 'power': 30, 'life': 40})
