# Flask - Création d'un serveur web

Explore les bases du développement web avec Flask, un micro-framework pour Python.

## Ressources

- [Flask - Installation](https://flask.palletsprojects.com/en/2.0.x/installation/)
- [Flask - Quickstart](https://flask.palletsprojects.com/en/2.0.x/quickstart/)
- [Debugging Application Errors](https://flask.palletsprojects.com/en/2.3.x/debugging/)

## Contexte du projet

Flask est un micro-framework pour Python. Il est considéré comme "micro" car il est simple et léger, tout en offrant un ensemble complet d'outils pour créer des applications web et des API. Flask est populaire pour sa simplicité et sa flexibilité dans la création de serveurs web et d'API.

### Qu'est-ce que Flask ?

Flask est une bibliothèque pour Python qui facilite la création de serveurs web. Il rend simple le traitement des requêtes HTTP et la gestion des routes, permettant ainsi de développer des applications web et des API de manière rapide et efficace.

### Qu'est-ce qu'un serveur web ?

Un serveur web est un logiciel qui utilise le protocole HTTP pour servir des fichiers et des données en réponse à des requêtes faites par un client, typiquement un navigateur web. Quand tu tapes une adresse dans ton navigateur, c'est un serveur web qui traite cette demande et renvoie la page ou les données demandées.

### Installation de Flask

Pour commencer, tu dois installer Flask dans ton environnement Python :

```bash
# création et ouverture du dossier du projet
mkdir flask-demo
cd flask-demo

# création et activation d'un environnement virtuel

# - pour Linux et MacOS
python3 -m venv venv
source venv/bin/activate

# - pour Windows
python -m venv venv
source venv/Scripts/activate

# installation de Flask
pip install Flask
```

#### Environnement Virtuel en Python

Un environnement virtuel est un espace isolé dans lequel tu peux installer des bibliothèques et des dépendances pour un projet spécifique sans affecter les autres projets. Cela permet de gérer les versions de chaque bibliothèque de manière indépendante. Utiliser un environnement virtuel est recommandé pour maintenir tes projets organisés et éviter les conflits entre les dépendances.

### Création d'une route

Créons un serveur Flask qui répond à une requête sur une route spécifique. Crée un fichier Python, par exemple, `app.py` :

```python
# importe le module Flask
from flask import Flask

app = Flask(__name__)

# définition d'une route GET sur l'URL racine ('/')
@app.route('/')
def hello_world():
    # envoie une réponse 'Hello World!' au client
    return 'Hello, World!'
```

Dans cet exemple, lorsque le serveur reçoit une requête GET à l'adresse "/", il répond avec le texte 'Hello, World!'. Le serveur écoute maintenant sur le port 5000.

Lance le serveur avec la commande `flask run`.

Ensuite, accède à la route depuis ton navigateur à l'adresse [http://localhost:5000/](http://localhost:5000/).

### Route Retournant un Objet (JSON)

Les API modernes utilisent souvent le format JSON pour échanger des données. Flask gère facilement cela :

```python
from flask import jsonify

@app.route('/data')
def data():
    return jsonify({'name': 'Pikachu', 'power': 20, 'life': 50})
```

Si tu apportes une modification à ton script, il te faudra relancer le serveur : dans un premier temps, ferme le serveur (CTRL-C), puis retappe la commande précédente `flask run`.

Dans cet exemple, une requête GET à l'adresse [http://localhost:5000/data](http://localhost:5000/data) retourne un objet au format JSON :

```json
{
  "name": "Pikachu",
  "power": 20,
  "life": 50
}
```

Le format JSON est le standard pour les API car il est facile à lire et à analyser pour les humains et les machines.

### Utilisation du mode debug

Le mode debug de Flask est un outil extrêmement utile pour le développement de ton application, il apporte les avantages suivants :

- **Relance automatique du serveur :** Flask va automatiquement redémarrer le serveur chaque fois que tu modifies un fichier de ton projet.

- **Messages d'erreur détaillés :** si une erreur se produit, Flask affichera une page d'erreur dans ton navigateur.

Pour activer le mode debug, tu peux utiliser la commande suivante lorsque tu lances ton application :

```bash
flask run --debug
```

## Modalités pédagogiques

- Crée un projet utilisant Flask pour mettre en place un serveur web simple.
- Développe des routes qui renvoient du texte et des objets JSON.
- Documente ton code pour expliquer comment chaque route fonctionne.
- Utilise le mode debug.
- Partage ton projet sur un dépôt GitLab.

## Livrables

Un lien vers GitLab

## Critères de performance

- Le code source est documenté
- Utiliser les normes de codage du langage
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions